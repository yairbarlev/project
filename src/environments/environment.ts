// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  dialogflow: {
    angularBot:"69e044f34082a9368592f252e6b5aab8da73753c",
  
    },
  firebaseConfig : {
    apiKey: "AIzaSyCsqoUL-SbudVqfhlbF0Ifd5DQ6APAztnE",
    authDomain: "angular-2021-oyoya3.firebaseapp.com",
    projectId: "angular-2021-oyoya3",
    storageBucket: "angular-2021-oyoya3.appspot.com",
    messagingSenderId: "653778249091",
    appId: "1:653778249091:web:fc6319834b1617c1e282b6"
  }  
};

 
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
