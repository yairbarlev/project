import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Employee } from '../interfaces/employee';
import { BehaviorSubject, Observable, observable, Subscription } from 'rxjs';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { PersonFormDialogComponent }  from '../person-form-dialog/person-form-dialog.component';
import { EmployeeService } from '../employee.service';
import { AuthService } from '../auth.service';
import { map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-table',
  templateUrl: './employee-table.component.html',
  styleUrls: ['./employee-table.component.css'],
})
export class EmployeeTableComponent implements OnInit {
  @ViewChild(MatPaginator)   paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  userId:string;
  employees$: BehaviorSubject<Employee[]>;
  public displayedColumns: string[] = ['firstName', 'lastName', 'age','id','gender','trainingHours'];

  public columnsToDisplay: string[] = [...this.displayedColumns, 'actions'];

  employeess$;
  employees:any  [];
  public columnsFilters = {};

  constructor(public authService:AuthService ,private router:Router,private EmployeesService: EmployeeService, public dialog: MatDialog) {
    this.dataSource = new MatTableDataSource<Employee>();
    this.employees$ = new BehaviorSubject([]);
  }   
  employeePage(employee:Employee){
    this.router.navigate(['/employee/',employee.age]); 
  }
 
  public dataSource: MatTableDataSource<Employee>;
  private serviceSubscribe: Subscription;

  edit(data: Employee) {
    // const dialogRef = this.dialog.open(PersonFormDialogComponent, {
    //   width: '400px',
    //   data: data
    // });

    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.EmployeesService.edit(this.userId,data);
    //   }
    // });
  }

  delete(id: any) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.EmployeesService.deleteEmployee(this.userId,id);
      }
    });
  }

  
  /**
   * initialize data-table by providing persons list to the dataSource.
   */
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId + "sgsg");
        this.employeess$ = this.EmployeesService.getEmployees2(this.userId); 
  
        this.employeess$.subscribe(
          docs =>{
            console.log('init worked');
            console.log(docs );
            this.dataSource = new MatTableDataSource<Employee>();
            this.dataSource.data = [];
            for(let document of docs){
              const employee:Employee = document.payload.doc.data();
              employee.id = document.payload.doc.id; 

              this.dataSource.data.push(employee);
              this.employees$.next(this.dataSource.data) 
              this.serviceSubscribe=this.employees$.subscribe(res => {
                this.dataSource.data = res;
              });

            }
            this.ngAfterViewInit();
          }
        ) 
      }
    )

  }
  ngAfterViewInit(): void {
    console.log('paginate');
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

  }

   ngOnDestroy(): void {
     this.serviceSubscribe.unsubscribe();
   }

  private filter() {

    this.dataSource.filterPredicate = (data: Employee, filter: string) => {
  
      let find = true;
  
      for (var columnName in this.columnsFilters) {
  
        let currentData = "" + data[columnName];
  
        //if there is no filter, jump to next loop, otherwise do the filter.
        if (!this.columnsFilters[columnName]) {
          return;
        }
  
        let searchValue = this.columnsFilters[columnName]["contains"];
  
        if (!!searchValue && currentData.indexOf("" + searchValue) < 0) {
  
          find = false;
          //exit loop
          return;
        }
  
        searchValue = this.columnsFilters[columnName]["equals"];
  
        if (!!searchValue && currentData != searchValue) {
          find = false;
          //exit loop
          return;
        }
  
        searchValue = this.columnsFilters[columnName]["greaterThan"];
  
        if (!!searchValue && currentData <= searchValue) {
          find = false;
          //exit loop
          return;
        }
  
        searchValue = this.columnsFilters[columnName]["lessThan"];
  
        if (!!searchValue && currentData >= searchValue) {
          find = false;
          //exit loop
          return;
        }
  
        searchValue = this.columnsFilters[columnName]["startWith"];
  
        if (!!searchValue && !currentData.startsWith("" + searchValue)) {
          find = false;
          //exit loop
          return;
        }
  
        searchValue = this.columnsFilters[columnName]["endWith"];
  
        if (!!searchValue && !currentData.endsWith("" + searchValue)) {
          find = false;
          //exit loop
          return;
        }
      }
      return find;
  
    };
  
    this.dataSource.filter = null;
    this.dataSource.filter = 'activate';
  
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
  /**
  
   * Create a filter for the column name and operate the filter action.
  
   */
  
  applyFilter(columnName: string, operationType: string, searchValue: string) {
  
    this.columnsFilters[columnName] = {};
    this.columnsFilters[columnName][operationType] = searchValue;
    this.filter();
  }
  
  /**
  
   * clear all associated filters for column name.
  
   */
  
  clearFilter(columnName: string) {
    if (this.columnsFilters[columnName]) {
      delete this.columnsFilters[columnName];
      this.filter();
    }
  }
}

