import { Time } from '@angular/common';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Employee } from './interfaces/employee';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  employees$: BehaviorSubject<Employee[]>;
  employees: Array<Employee> = [];

  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  employeeCollection:AngularFirestoreCollection;

  public getEmployees2(userId){
    this.employeeCollection = this.db.collection(`users/${userId}/employees`); 
    return this.employeeCollection.snapshotChanges()
    
  } 
  public getEmployee(userId,emplyeeId){
    return  this.db.collection(`users/${userId}/employees`).doc(`/${emplyeeId}`).get();  
        } 

  public getEmployees(userId){
    this.employeeCollection = this.db.collection(`users/${userId}/employees`);
    return this.employeeCollection.snapshotChanges()      
  } 
  deleteEmployee(Userid:string, id:string){
    this.db.doc(`users/${Userid}/employees/${id}`).delete(); 
  } 
  edit(userId:string,data: Employee){
    this.db.doc(`users/${userId}/employees/${data.id}`).update(
      {
      gender:data.gender,  
      firstName:data.firstName,
      lastName:data.lastName,
      rExperience:data.rExperience, 
      educationLevel:data.educationLevel,
      enrollment:data.enrollment,
      majorDiscipline:data.majorDiscipline, 
      companySize:data.companySize,
      companytype:data.companytype, 
      educationYears:data.educationYears,
      experienceYears:data.experienceYears,
      yearsInCurrentJob:data.yearsInCurrentJob,
      trainingHours:data.trainingHours,
      city:data.city,
      street:data.street, 
      state:data.state,
      age:data.age,
      result:data.result
      }
    )
  } 
  updateEmployee(userId:string,employee:Employee){
      console.log("save");
    this.db.doc(`users/${userId}/employees/${employee.id}`).update(
      {
      gender:employee.gender,  
      firstName:employee.firstName,
      lastName:employee.lastName,
      rExperience:employee.rExperience, 
      educationLevel:employee.educationLevel,
      enrollment:employee.enrollment,
      majorDiscipline:employee.majorDiscipline, 
      companySize:employee.companySize,
      companytype:employee.companytype, 
      educationYears:employee.educationYears,
      experienceYears:employee.experienceYears,
      yearsInCurrentJob:employee.yearsInCurrentJob,
      trainingHours:employee.trainingHours,
      city:employee.city,
      street:employee.street, 
      state:employee.state,
      age:employee.age,
      }
    )
  }
  addEmployee(userId:string,gender:string,  
    firstName:string,
    lastName:string,
    rExperience:string, 
    educationLevel:string,
    enrollment:string,
    majorDiscipline:string, 
    companySize:number,
    companytype:string, 
    educationYears:number,
    experienceYears:number,
    yearsInCurrentJob:number,
    trainingHours:number,
    city:string,
    street:string, 
    state:string,
    age:number,
    answer:string)
    {
      if(userId!=null){
    const employee = {gender:gender,  
      firstName:firstName,
      lastName:lastName,
      rExperience:rExperience, 
      educationLevel:educationLevel,
      enrollment:enrollment,
      majorDiscipline:majorDiscipline, 
      companySize:companySize,
      companytype:companytype, 
      educationYears:educationYears,
      experienceYears:experienceYears,
      yearsInCurrentJob:yearsInCurrentJob,
      trainingHours:trainingHours,
      city:city,
      street:street, 
      state:state,
      age:age,
      answer:answer}; 
    this.userCollection.doc(userId).collection('employees').add(employee).then(function(docRef) {
      console.log("Document written with ID: ", docRef.id);
      return(docRef.id);
  })
  .catch(function(error) {
      console.error("Error adding document: ", error);
      return(error);
  });;
      }
  }

  constructor(private db:AngularFirestore) { 
    this.employees$ = new BehaviorSubject([]);
    // this.employees = personsData;
  }
}
