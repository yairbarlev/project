import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-middle',
  templateUrl: './middle.component.html',
  styleUrls: ['./middle.component.css']
})
export class MiddleComponent implements OnInit {
  userId:string;
  userEmail:string;
  companySize:number;
  companyType:string;
  constructor(private route:ActivatedRoute,public authService:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId+ " bye"); 
        this.companyType = this.route.snapshot.params.companyType; 
        this.companySize = this.route.snapshot.params.companySize;
        this.authService.addAccount(this.userId,this.companyType,this.companySize)
        console.log(this.userEmail);  
      }
    )
    setTimeout(() => {
      this.router.navigate(['/account/']);  
  }, 2000);

  }

}
