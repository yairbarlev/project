import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { EmployeeService } from '../employee.service';
import { Employee } from '../interfaces/employee';
import { ClassifyService } from './../classify.service';
import { FormGroup } from '@angular/forms';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
@Component({
  selector: 'employeeForm',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Input() gender:any;  
  @Input() firstName:string; 
  @Input() lastName:string; 
  @Input() rExperience:any; 
  @Input() educationLevel:any;
  @Input() enrollment:any;  
  @Input() majorDiscipline:string; 
  @Input() companySize:number; 
  @Input() companytype:string; 
  @Input() educationYears:number;
  @Input() experienceYears:number;
  @Input() yearsInCurrentJob:number;
  @Input() trainingHours:number;
  @Input() city:string; 
  @Input() street:string; 
  @Input() state:string;
  @Input() age:number;


  //fake answer is used to avoid using the endpoint
  fakeanswer = "Dying to Leave";
  category:number = 0;
  genders: string[] = ['Male', 'Female', 'Other'];
  rExperiences: string[] = ['No relevent experience', 'Has relevent experience'];
  Educations: string[] = ['Phd', 'Masters','Graduate','other'];
  enrollments: string[] = ['no_enrollment', 'Full time course','Part time course'];
  userId:any;
  res:any;

	// isLinearvarient = false;
  varientfirstFormGroup: FormGroup=Object.create(null);
 	varientsecondFormGroup: FormGroup=Object.create(null);
  employeeForm: FormGroup=Object.create(null);


  constructor( private router:Router, private fb: FormBuilder,private employeeService:EmployeeService
    ,private authService:AuthService,private classifyService:ClassifyService,private afAuth:AngularFireAuth) { }
 
  submit(){
    
    if(this.gender=='Male'){
      this.gender=2
    }
    else{
      if(this.gender=='Female'){
        this.gender=1
      }
      else{
        this.gender=0
      }
    }

    if(this.rExperience=='No relevent experience'){
      this.rExperience=0
    }
    else{
        this.rExperience=1
    }
    if(this.educationLevel=='Phd'){
      this.educationLevel=0
    }
    else{
      if(this.educationLevel=='Masters'){
        this.educationLevel=1
      }
      else{
        this.educationLevel=2
      }
    }
    if(this.enrollment=='no_enrollment'){
      this.educationLevel=2
    }
    else{
      if(this.enrollment=='Full time course'){
        this.enrollment=1
      }
      else{
        this.enrollment=0
      }
    }

    console.log(this.gender + "gender")
    console.log(this.firstName + "firstName")
    console.log(this.lastName + "lastName")
    console.log(this.rExperience + "rExperience")
    console.log(this.educationLevel + "educationLevel")
    console.log(this.enrollment + "enrollment")
    console.log(this.majorDiscipline + "majorDiscipline")
    console.log(this.companySize + "companySize")
    console.log(this.companytype + "companytype")
    console.log(this.educationYears + "educationYears")
    console.log(this.experienceYears + "experienceYears")
    console.log(this.yearsInCurrentJob + "yearsInCurrentJob")
    console.log(this.trainingHours + "trainingHours")
    console.log(this.city + "city")
    console.log(this.street + "street")
    console.log(this.state + "state")

    let employee:Employee = {gender:this.gender, firstName:this.firstName, lastName:this.lastName,
      rExperience:this.rExperience, educationLevel:this.educationLevel, 
      enrollment:this.enrollment, majorDiscipline:this.majorDiscipline, 
      companySize:this.companySize, companytype:this.companytype, 
      educationYears:this.educationYears, experienceYears:this.experienceYears
      , yearsInCurrentJob:this.yearsInCurrentJob, trainingHours:this.trainingHours, city:this.city, 
      street:this.street, 
      state:this.state,age:this.age,result:this.fakeanswer};
      
    this.classifyService.classify(employee).subscribe(
      res => {
        console.log(res);
        this.category = this.classifyService.categories[res];  
      }
    );

    
       if(this.afAuth.authState ){
        this.res = this.employeeService.addEmployee(this.userId,this.gender, this.firstName, this.lastName,
          this.rExperience, this.educationLevel, 
          this.enrollment, this.majorDiscipline, 
          this.companySize, this.companytype, 
          this.educationYears, this.experienceYears
          , this.yearsInCurrentJob, this.trainingHours, this.city, 
          this.street, 
          this.state,this.age,this.fakeanswer);
      }
      
      if(this.afAuth.authState!=null ){
        // this.router.navigate(['/middle/',this.companytype, this.companySize]); 
        this.router.navigate(['/result/',this.res]); 
       }
       else{
        this.router.navigate(['/result/',this.res]); 
       }

  }
  
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
         
        
        
      }
    )

		// varient
		this.varientfirstFormGroup = this.fb.group({
		      varientfirstCtrl: ['', Validators.required]
		});
		this.varientsecondFormGroup = this.fb.group({
		      varientsecondCtrl: ['', Validators.required]
		});
	
	}
}


