
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '../auth.service';
import { EmployeeService } from '../employee.service';
import { Employee } from '../interfaces/employee';
@Component({
  selector: 'app-person-form-dialog',
  templateUrl: './person-form-dialog.component.html',
  styleUrls: ['./person-form-dialog.component.css']
})
export class PersonFormDialogComponent implements OnInit {
  // formInstance: FormGroup;
  formInstance: FormGroup=Object.create(null);
  userId:string;
  @Input() id:any; 
  @Input() gender:any;  
  @Input() firstName:string; 
  @Input() lastName:string; 
  @Input() rExperience:any; 
  @Input() educationLevel:any;
  @Input() enrollment:any;  
  @Input() majorDiscipline:string; 
  @Input() companySize:number; 
  @Input() companytype:string; 
  @Input() educationYears:number;
  @Input() experienceYears:number;
  @Input() yearsInCurrentJob:number;
  @Input() trainingHours:number;
  @Input() city:string; 
  @Input() street:string; 
  @Input() state:string;
  @Input() age:number;
  @Output() saves = new EventEmitter<Employee>();

  // public dialogRef: MatDialogRef<PersonFormDialogComponent>,
  constructor(  private fb: FormBuilder,
     public authService:AuthService,private employeeService:EmployeeService) {
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
      }
    )
  }

  save(): void {
    let employee:Employee = {
      id:this.id,
      gender:this.gender,  
      firstName:this.firstName,
      lastName:this.lastName,
      rExperience:this.rExperience, 
      educationLevel:this.educationLevel,
      enrollment:this.enrollment,
      majorDiscipline:this.majorDiscipline, 
      companySize:this.companySize,
      companytype:this.companytype, 
      educationYears:this.educationYears,
      experienceYears:this.experienceYears,
      yearsInCurrentJob:this.yearsInCurrentJob,
      trainingHours:this.trainingHours,
      city:this.city,
      street:this.street, 
      state:this.state,
      age:this.age};
    this.employeeService.updateEmployee(this.userId,employee);
      console.log("save");
  }
}