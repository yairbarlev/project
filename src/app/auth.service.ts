import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  res:any;
  userId:any;
  user:Observable<User | null>; 
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  SignUp(email:string, password:string){
    this.res = this.afAuth.createUserWithEmailAndPassword(email,password);
    return this.res
  }
  
  
  login(email:string, password:string){
    return this.afAuth
        .signInWithEmailAndPassword(email,password)   
  }
  addAccount(userId:string,companyType:string,companySize:number){
    const acount = {companyType:companyType,companySize:companySize}; 
    console.log(userId + 'hey');
    this.userCollection.doc(userId).collection('acounts').add(acount);
  }
  logout(){
    this.afAuth.signOut(); 
  }

  getUser():Observable<User | null>  {
    return this.user; 
  }

  constructor(private afAuth:AngularFireAuth, private router:Router,private db:AngularFirestore) { 
    this.user = this.afAuth.authState; 
  }
}
