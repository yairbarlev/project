import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  email:any = "";
  afAuth: any;
  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.user.subscribe(
          user => {
            this.email =user?.email;
      }
    )

  }

}
