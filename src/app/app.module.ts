import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'; 


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';



import { MatListModule } from '@angular/material/list';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { ClassifyComponent } from './classify/classify.component';


import { HttpClientModule } from '@angular/common/http';
import { CityFormComponent } from './city-form/city-form.component';
import { PostsComponent } from './posts/posts.component';

import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { BookFormComponent } from './book-form/book-form.component';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component'; 

//project changes
import { ReactiveFormsModule } from '@angular/forms';
import { ResultComponent } from './result/result.component';
import { MiddleComponent } from './middle/middle.component';
import { AccountComponent } from './account/account.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


import {MatAutocompleteModule} from '@angular/material/Autocomplete';

// import {MatButtonToggleModule} from '@angular/material/ButtonToggle';

import {MatCheckboxModule} from '@angular/material/Checkbox';
import {MatChipsModule} from '@angular/material/Chips';
// import {MatModule} from '@angular/material/';
import {MatDatepickerModule} from '@angular/material/Datepicker';
import {MatDialogModule} from '@angular/material/Dialog';

// import {MatFormFieldModule} from '@angular/material/FormField';
// import {MatGridListModule} from '@angular/material/GridList';
import { ChatModule } from './chat/chat.module';
import {MatStepperModule} from '@angular/material/Stepper';
import {MatTooltipModule} from '@angular/material/Tooltip';

import {MatTabsModule} from '@angular/material/Tabs';
import {MatTableModule} from '@angular/material/Table';
import {MatSortModule} from '@angular/material/Sort';
// import {MatSnackBarModule} from '@angular/material/SnackBar';
// import {MatSlideToggleModule} from '@angular/material/SlideToggle';
import {MatSliderModule} from '@angular/material/Slider';

// import {MatProgressSpinnerModule} from '@angular/material/ProgressSpinner';
// import {MatProgressBarModule} from '@angular/material/ProgressBar';
import {MatPaginatorModule} from '@angular/material/Paginator';
import {MatMenuModule} from '@angular/material/Menu';

// import {MatRippleModule} from '@angular/material/Ripple';


// import {MatNativeDateModule} from '@angular/material/NativeDate';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkAccordionModule} from '@angular/cdk/accordion';
import {A11yModule} from '@angular/cdk/a11y';
import {BidiModule} from '@angular/cdk/bidi';
import {OverlayModule} from '@angular/cdk/overlay';
import {PlatformModule} from '@angular/cdk/platform';
import {ObserversModule} from '@angular/cdk/observers';
import {PortalModule} from '@angular/cdk/portal';

import { NbThemeModule, NbLayoutModule, NbChatModule, NbSpinnerModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { ChatbotComponent } from './chatbot/chatbot.component';
import { SalesComponent } from './sales/sales.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { EmployeeTableComponent } from './employee-table/employee-table.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { PersonFormDialogComponent } from './person-form-dialog/person-form-dialog.component';
import { DashComponent } from './dash/dash.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { EmployeeComponent } from './employee/employee.component';



@NgModule({

  declarations: [
    AppComponent,
    BooksComponent,
    NavComponent,
    TemperaturesComponent,
    ClassifyComponent,
    CityFormComponent,
    PostsComponent,
    LoginComponent,
    SignUpComponent,
    BookFormComponent,
    HomeComponent,
    FormComponent,
    ResultComponent,
    MiddleComponent,
    AccountComponent,
    SalesComponent,
    UserProfileComponent,
    EmployeeTableComponent,
    ConfirmationDialogComponent, 
    PersonFormDialogComponent, DashComponent, EmployeeComponent ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ChatModule,
    CdkTableModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    BrowserModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbChatModule,
    NbSpinnerModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatInputModule,
    FormsModule,
    MatRadioModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    // MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatTableModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    // MatFormFieldModule,
    // MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    // MatProgressBarModule,
    // MatProgressSpinnerModule,
    MatRadioModule,
    // MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    // MatSlideToggleModule,
    MatSliderModule,
    // MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    // MatNativeDateModule,
    CdkTableModule,
    A11yModule,
    BidiModule,
    CdkAccordionModule,
    ObserversModule,
    OverlayModule,
    PlatformModule,
    PortalModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFirestoreModule,
    MatGridListModule
  ],
  entryComponents: [ConfirmationDialogComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
