import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Employee } from './interfaces/employee';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  private url = "https://x62ychb5gc.execute-api.us-east-1.amazonaws.com/beta";

  categories:object = {0:'Business', 1:'Entertainment', 2:'Politics', 3:'Sport', 4:'Tech'};

  classify(employee:Employee){
    let json = {'emp_details':[
      {
        "city_development_index": 0.776,
        "gender": employee.gender,
        "relevent_experience": employee.rExperience,
        "enrolled_university": employee.enrollment,
        "education_level": employee.educationLevel,
        "major_discipline": employee.majorDiscipline,
        "experience": employee.experienceYears,
        "company_size": employee.companySize,
        "company_type": employee.companytype,
        "last_new_job": employee.yearsInCurrentJob,
        "training_hours": employee.trainingHours
      }
    ]}
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        let final:string = res.body;
        console.log(final);
        final = final.replace('[',''); 
        final = final.replace(']','');
        return final; 
      })
    )

  }
  // classify(text:string){
  //   let json = {'articles':[
  //     {'text':text}
  //   ]}
  //   let body = JSON.stringify(json);
  //   return this.http.post<any>(this.url, body).pipe(
  //     map(res => {
  //       console.log(res);
  //       let final:string = res.body;
  //       console.log(final);
  //       final = final.replace('[',''); 
  //       final = final.replace(']','');
  //       return final; 
  //     })
  //   )

  // }
  constructor(private http:HttpClient) { }
}
